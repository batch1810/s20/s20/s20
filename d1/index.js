/* -JSON// console.log("Hello World.");

// JSON Objects
/*
	- JSON stands for JavaScript Object Notation
	- A common use of JSON is to read data from the web server, and display the data in a webpage.
	- Reason why JSON?
		- It is a lightweight data-interchange format.
		- It is easy for humans to read and write.
		- It is easy for machines to parse and generate.
*/

// [SECTION] JSON Object
/*
	- JSON also use the "key/value pairs" just like object properties in JavaScript.
		- The main difference is "key/property" is enclosed with double quotes.
	- JSON data that is sent/received is in text-only (String) format.
	-Syntax:
	{
		"propertyA": "valueA",
		"propertyB": "valueB"
	}

*/

// {
// 	"city":"Quezon City",
// 	"province": "Metro Manila",
// 	"country": "Philippines"
// }

// [SECTION] JSON Arrays
// Array of Objects
// "cities": [
// 	{"city": "Quezon City", "province":"Metro Manila", "country": "Philippines"},
// 	{"city": "Manila City", "province":"Metro Manila", "country": "Philippines"},
// 	{"city": "Makati City", "province":"Metro Manila", "country": "Philippines"}
// ]

// [SECTION] JSON Methods
/*
	- JSON Object contains methods for parsing and converting data into stringified JSON.
	- JSON data is sent or received in text-only format (JSON).
*/

// [SECTION] Converting Data Into Stringified JSON
/*
	- Stringified JSON is a JavaScript object converted into a JSON String.
	- They are commonly used for HTTP Request where information is required to be sent/received in stringified JSON format.

*/

let batchesArr = [
	
	{batchName: "Batch 183"},
	{batchName: "Batch 184"}

];

console.log(batchesArr);

// "stringify" method is used to convert JavaScript objects into a string
console.log("Result from stringify method:");
// JSON.stringify(objVariable);
console.log(JSON.stringify(batchesArr));

// Converting JSON before storing it in a variable
let data = JSON.stringify({
	name: "John",
	age: 31,
	address: {
		city: "Manila",
		country: "Philippines"
	}
})

console.log(data);

// [SECTION] Using Stringify Method With Variables

// User Details
// let firstName = prompt("What is your first name?");
// let lastName = prompt("What is your last name?");
// let age = prompt("What is your age?");
// let address ={
// 	city: prompt("Which city do you live in?"),
// 	country: prompt("Which country does your city belong to?")
// };

// stringify method is used upon receiving the information from the user input.
// let otherData = JSON.stringify({
// 	firstName: firstName,
// 	lastName: lastName,
// 	age: age,
// 	address: address
// })

// console.log(otherData);

//[SECTION] Converting Stringified JSON Into JavaScript Objects

let batchesJSON = `[
	{"batchName": "Batch 183"},
	{"batchName": "Batch 184"}
]`;

console.log(batchesJSON);
console.log(batchesJSON[0]);

//JSON.parse Method
// It converts JSON Objects into JavaScript Objects
// JSON.parse(JSON Object);

console.log("Result from parse method:");
console.log(JSON.parse(batchesJSON)); 

// contain parse object in a variable
let parseBatches = JSON.parse(batchesJSON);

console.log(parseBatches[0]);

let stringifiedObject = `{
	"name" : "John",
	"age" : 31,
	"address":{
		"city":"Manila",
		"country": "Philippines"
	}
}`

console.log(stringifiedObject);

console.log(JSON.parse(stringifiedObject));
